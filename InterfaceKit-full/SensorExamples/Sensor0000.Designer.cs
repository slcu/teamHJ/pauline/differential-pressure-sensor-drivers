namespace InterfaceKit_full.SensorExamples
{
    partial class Sensor0000
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.analogInputLabel0 = new System.Windows.Forms.Label();
            this.buttonAcquisition = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(51, 9);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox1.MaxLength = 3;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(125, 22);
            this.textBox1.TabIndex = 51;
            // 
            // analogInputLabel0
            // 
            this.analogInputLabel0.AutoSize = true;
            this.analogInputLabel0.Location = new System.Drawing.Point(7, 12);
            this.analogInputLabel0.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.analogInputLabel0.Name = "analogInputLabel0";
            this.analogInputLabel0.Size = new System.Drawing.Size(39, 17);
            this.analogInputLabel0.TabIndex = 53;
            this.analogInputLabel0.Text = "Raw:";
            // 
            // buttonAcquisition
            // 
            this.buttonAcquisition.Location = new System.Drawing.Point(282, 38);
            this.buttonAcquisition.Name = "buttonAcquisition";
            this.buttonAcquisition.Size = new System.Drawing.Size(110, 35);
            this.buttonAcquisition.TabIndex = 58;
            this.buttonAcquisition.Text = "Acquisition";
            this.buttonAcquisition.UseVisualStyleBackColor = true;
            // 
            // Sensor0000
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buttonAcquisition);
            this.Controls.Add(this.analogInputLabel0);
            this.Controls.Add(this.textBox1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Sensor0000";
            this.Size = new System.Drawing.Size(423, 80);
            this.Click += new System.EventHandler(this.buttonAcquisition_Click);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label analogInputLabel0;
        private System.Windows.Forms.Button buttonAcquisition;
    }
}
